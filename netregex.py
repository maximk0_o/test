#!/usr/bin/python

import re

def range_help(a,b):
    if a[:-1] == b[:-1]:
        return '{0}[{1}-{2}]'.format(a[:-1], a[-1], b[-1])
    elif int(a) < 10:
        if int(b[-2]) == 1:
            return '([{0}-9]|1[0-{1}])'.format(a[0], b[-1])
        elif int(b[-2]) == 2:
            return '([{0}-9]|1[0-9]|2[0-{1}])'.format(a[0], b[-1])
        else:
            return '([{0}-9]|[1-{1}][0-9]|{2}[0-{3}])'.format(a[-1], \
                                   str(int(b[-2]) - 1), b[-2], b[-1])

    elif int(b[-2]) - int(a[-2]) == 1:
        return '({0}[{1}-9]|{2}[0-{3}])'.format(a[:-1], a[-1], b[:-1], b[-1])
    elif int(b[-2]) - int(a[-2]) == 2:
        return '({0}{1}[{2}-9]|{0}{3}[0-9]|{4}{5}[0-{6}])'.format(\
                a[:-2], a[-2], a[-1], str(int(a[-2]) + 1), b[:-2], \
                b[-2], b[-1])
    else:
        return '({0}{1}[{2}-9]|{0}[{3}-{4}][0-9]|{5}{6}[0-{7}])'.format(\
                a[:-2], a[-2], a[-1], str(int(a[-2]) + 1), \
                str(int(b[-2]) - 1), b[:-2], b[-2], b[-1])


def range_to_regex(a,b):
    a = str(a)
    b = str(b)
    az = a.zfill(3)
    bz = b.zfill(3)

    if az[:-1] == bz[:-1]:
        return '{0}[{1}-{2}]'.format(a[:-1], a[-1], b[-1])
    elif az[0] == bz[0]:
        return range_help(a,b)
    else:
        if int(bz[0]) - int(az[0]) == 1:
            return "({0}|{1})".format(range_help(a, a[:-2] + '99'), \
                             range_help(b[:-2] + '00', b))
        elif int(bz[0]) - int(az[0]) == 2:
            return "({0}|1[0-9]\{2\}|{1})" % range_help(a, '99'), \
                                       range_help(b[:-2] + '00', b)


def get_network_regex(net):
    ip, mask = net.split('/')
    ip = [int(x) for x in ip.split('.')]
    mask = int(mask)
    regex = ''
    octet = '(2[0-4][0-9]|25[0-5]|1?[0-9]{1,2})\.'
    
    for i in range(4):
        if i < mask // 8:
            regex += str(ip[i]) + '\.'
        elif i == mask // 8 and mask % 8 != 0:
            b = bin(ip[mask // 8]).split('b')[1].zfill(8)
            m = mask % 8
            bMin = int(b[0:m] + '0' * (8 - m), 2)
            bMax = int(b[0:m] + '1' * (8 - m), 2)
            bMins = str(bMin)
            bMaxs = str(bMax)

            regex += range_to_regex(bMin, bMax) + '\.'  
        else:
            regex += octet
    
    return regex[:-2]


def networks_to_regex(nets):
    netRegex = '^'
    isNet = re.compile('^(2[0-4][0-9]|25[0-5]|1?[0-9]{1,2}\.){3}(2[0-4][0-9]|25[0-5]|1?[0-9]{1,2})/(3[02]|[1-2]?\d)$')
    isIp = re.compile('^(2[0-4][0-9]|25[0-5]|1?[0-9]{1,2}\.){3}(2[0-4][0-9]|25[0-5]|1?[0-9]{1,2})$')
    if not len(nets):
        return isIp

    for i in nets:
        if isNet.match(i):
            netRegex += "({0})|".format(get_network_regex(i))
        elif isIp.match(i):
            netRegex += "({0})|".format(i.replace('.','\.'))
        else:
            raise ValueError('Error local network format: ' + str(i))

    print(netRegex[:-1] + '$')
    return re.compile(netRegex[:-1] + '$')

