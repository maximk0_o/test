#!/usr/bin/python

import re
from scapy.all import *
from netregex import networks_to_regex

def main():
    a = rdpcap("test.dump")
    
    ports = range(1,1024)
    local_nets = ['195.54.2.141', '192.168.0.0/16',
                  '10.0.0.0/8', '195.54.14.0/23']
    netreg = networks_to_regex(local_nets)
    b = [pkt for pkt in a if TCP in pkt and
             (pkt[TCP].sport in ports or pkt[TCP].dport in ports) and netreg.match(pkt[IP].src)]
    
    for pkt in b:
        print pkt.sprintf("%.time%: %IP.src%:%dr,TCP.sport%\t->\t%IP.dst%:%dr,TCP.dport%")

if __name__ == "__main__":
    main()
