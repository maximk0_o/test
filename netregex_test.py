#!/usr/bin/python

import unittest
from netregex import networks_to_regex

class TestSequenceFunctions(unittest.TestCase):

    def setUp(self):
        self.seq = ['164.0.23.255/' + str(x) for x in range(1,30)]

    def test_networks_to_regex(self):
        for i in self.seq:
            regex = networks_to_regex([i])
            self.assertEqual(regex.match('0.123.54.2'),None)
        regex = networks_to_regex(['195.54.14.65/26', '195.54.2.141', '8.8.8.8/32'])
        self.assertNotEqual(regex.match('195.54.14.65'),None)
        self.assertNotEqual(regex.match('195.54.14.73'),None)
        self.assertNotEqual(regex.match('195.54.14.127'),None)
        self.assertNotEqual(regex.match('195.54.14.100'),None)
        self.assertNotEqual(regex.match('195.54.2.141'),None)
        self.assertNotEqual(regex.match('8.8.8.8'),None)
        self.assertRaises(ValueError, networks_to_regex, ['8.8.8.8/31'])
        
if __name__ == '__main__':
    unittest.main()
